package com.example.sunflowerfake


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.example.sunflowerfake.databinding.FragmentPlantListBinding


/**
 * A simple [Fragment] subclass.
 */
class PlantListFragment : Fragment() {

    private lateinit var viewModel : PlantListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =  ViewModelProviders.of(this).get(PlantListViewModel::class.java)
        val binding = FragmentPlantListBinding.inflate(inflater, container, false)

        context ?: return binding.root

        val adapter = PlantAdapter()
        binding.plantList.adapter = adapter

       subscribeUi(adapter)
        return binding.root
    }
    private fun subscribeUi(adapter: PlantAdapter) {
        viewModel.plants.observe(viewLifecycleOwner) { plants ->
            adapter.submitList(plants)
        }
    }





}
