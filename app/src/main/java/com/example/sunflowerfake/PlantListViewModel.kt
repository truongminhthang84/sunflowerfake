package com.example.sunflowerfake

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PlantListViewModel : ViewModel() {

    private val _plants = MutableLiveData<List<Plant>>(
        listOf(
            Plant(1, "Hoa Hong"),
            Plant(2, "Hoa Hue"),
            Plant(3, "Hoa Nhai"),
            Plant(4, "Hoa Xuyen chi")
        )
    )
    val plants: LiveData<List<Plant>>
        get() = _plants
}